package cli

import (
	"log"
	"net/url"

	_ "github.com/go-sql-driver/mysql"

	"github.com/cialldoin/genDBModels/run"
	"github.com/jmoiron/sqlx"
	flag "github.com/spf13/pflag"
)

var database = flag.String("database", "root:coding1@tcp(localhost:3306)/football", "mysql master")

func Run() int {
	flag.Parse()
	dsn := *database + "?parseTime=true&loc=" + url.QueryEscape("Europe/Moscow")

	db, err := sqlx.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("open mysql on address %s failed: %s", dsn, err)
	}
	run.Run(db)
	return 0
}
