package run

import (
	"fmt"
	"go/ast"
	"go/token"
	"log"

	"bitbucket.org/altmax/astHelp"

	"strings"

	"io/ioutil"

	"os"

	"github.com/jmoiron/sqlx"
)

type Table struct {
	Name string `db:"Tables_in_football"`
}
type Field struct {
	Name string
	Type string
	Attr string
}
type Strct struct {
	Name   string
	Fields []Field
}

type Column struct {
	Name    string      `db:"Field"`
	Type    []uint8     `db:"Type"`
	Null    interface{} `db:"Null"`
	Key     interface{} `db:"Key"`
	Default interface{} `db:"Default"`
	Extra   interface{} `db:"Extra"`
}

func Run(DB *sqlx.DB) int {
	// fmt.Println(astHelp.CreateBlockStmt())
	// type realColumn struct {
	// 	Name string
	// 	Type string
	// }
	var tables []Table
	var columns []Column
	// var realColumns []realColumn
	// var structs []Strct
	DB.Select(&tables, "show tables")
	// fmt.Println(tables)
	for _, v := range tables {
		// query := "show columns from ?"
		s := Strct{Name: strings.Title(v.Name)}
		columns = make([]Column, 0)
		// realColumns = make([]realColumn, 0)
		err := DB.Select(&columns, "show columns from "+v.Name)
		if err != nil {
			fmt.Println(err)
		}
		for _, v2 := range columns {
			// realColumns = append(realColumns, realColumn{Name: v.Name, Type: string(v.Type)})
			// name :=
			s.Fields = append(s.Fields, Field{Name: v2.Name, Type: string(v2.Type)})
		}
		// fmt.Println(realColumns)
		// structs = append(structs, s)
		decls := CreateDeclsByStructs([]Strct{s})
		field := CreateDBInterfaceField()
		st := astHelp.CreateStruct(strings.Title(v.Name)+"Table", []*ast.Field{field})
		decls = append(decls, st)
		cruds := CreateCRUDsByTable(v.Name)
		decls = append(decls, cruds...)
		b := CreateFileByDecls(decls)
		os.Mkdir("models", 0777)
		err = ioutil.WriteFile("models/"+astHelp.TransName(v.Name)+".go", b, 0777)
		if err != nil {
			log.Println(err)
		}
	}
	// fmt.Println(structs)
	// b := CreateFileByStructs(structs)
	// os.Mkdir("models", 0777)
	// err := ioutil.WriteFile("models/models.go", b, 0777)
	// if err != nil {
	// 	log.Println(err)
	// 	return 1
	// }
	return 0
}

func CreateCRUDsByTable(name string) []ast.Decl {
	insert := CreateInsertFunction(name)
	return []ast.Decl{insert}
}

func CreateInsertFunction(tableName string) *ast.FuncDecl {
	params := astHelp.CreateField(tableName, strings.Title(tableName))
	ret1 := astHelp.CreateField("", "int64")
	ret2 := astHelp.CreateField("", "error")
	results := []*ast.Field{ret1, ret2}
	name := "Insert"
	recv := astHelp.CreateField("this", "*"+strings.Title(tableName)+"Table")
	stmt := astHelp.CreateFuncStmt("println")
	stmt2 := astHelp.CreateBinaryStmt("query", "`insert into feed (mnemocode, format, obj_mnemo, ttl) values (?, ?, ?, ?);`")
	bstmt := astHelp.CreateBlockStmt([]ast.Stmt{stmt, stmt2})
	return astHelp.CreateFunction(nil, []*ast.Field{recv}, name, []*ast.Field{params}, results, bstmt)
}

func CreateDeclsByStructs(structs []Strct) []ast.Decl {
	decls := make([]ast.Decl, 0)
	for _, v := range structs {
		fields := make([]*ast.Field, 0)
		for _, v2 := range v.Fields {
			typ := DefineType(v2.Type)
			f := astHelp.CreateField(v2.Name, typ, "db")
			fields = append(fields, f)
		}
		s := astHelp.CreateStruct(v.Name, fields)
		decls = append(decls, s)
		// f := CreateDBInterfaceField()
		// s = astHelp.CreateStruct(strings.Title(v.Name)+"Table", []*ast.Field{f})
		// decls = append(decls, s)
	}
	return decls
}

func CreateFileByDecls(decls []ast.Decl) []byte {

	f := astHelp.CreateFile("models", decls)
	return astHelp.PrintAST(f)
}

func DefineType(str string) string {
	exp := strings.Index(str, "(")
	if exp != -1 {
		baseType := str[:exp]
		if baseType == "int" || baseType == "tinyint" {
			lng := str[exp+1 : len(str)-1]
			if lng == "1" {
				return "bool"
			}
			return "int64"
		}
		return "string"
	}
	return "string"
}

func CreateDBInterfaceField() *ast.Field {
	f := ast.Field{
		Names: []*ast.Ident{&ast.Ident{Name: "DB"}},
		Type: &ast.InterfaceType{
			Methods: &ast.FieldList{
				List: []*ast.Field{
					&ast.Field{
						Type: &ast.Ident{Name: "sqlx.Execer"},
					},
					&ast.Field{
						Names: []*ast.Ident{&ast.Ident{Name: "Get"}},
						Type: &ast.FuncType{
							Func: token.NoPos,
							Params: &ast.FieldList{
								List: []*ast.Field{
									&ast.Field{
										Type: &ast.Ident{Name: "interface{}"},
									},
									&ast.Field{
										Type: &ast.Ident{Name: "string"},
									},
									&ast.Field{
										Type: &ast.Ident{Name: "...interface{}"},
									},
								},
							},
							Results: &ast.FieldList{
								List: []*ast.Field{
									&ast.Field{
										Type: &ast.Ident{Name: "error"},
									},
								},
							},
						},
					},
					&ast.Field{
						Names: []*ast.Ident{&ast.Ident{Name: "Select"}},
						Type: &ast.FuncType{
							Func: token.NoPos,
							Params: &ast.FieldList{
								List: []*ast.Field{
									&ast.Field{
										Type: &ast.Ident{Name: "interface{}"},
									},
									&ast.Field{
										Type: &ast.Ident{Name: "string"},
									},
									&ast.Field{
										Type: &ast.Ident{Name: "...interface{}"},
									},
								},
							},
							Results: &ast.FieldList{
								List: []*ast.Field{
									&ast.Field{
										Type: &ast.Ident{Name: "error"},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	return &f
	// decls := make([]ast.Decl, 0)
	// s := astHelp.CreateStruct("test", []*ast.Field{&f})
	// decls = append(decls, s)
	// file := astHelp.CreateFile("models", decls)
	// astHelp.PrintAST(file)
}

// Type: &ast.FuncType{
// 							Func: token.NoPos,
// 							Params: &ast.FieldList{
// 								List: []*ast.Field{
// 									&ast.Field{
// 										Type: &ast.InterfaceType{},
// 									},
// 								},
// 							},
// 							Results: &ast.FieldList{
// 								List: []*ast.Field{
// 									&ast.Field{
// 										Type: &ast.Ident{Name: "error"},
// 									},
// 								},
// 							},
// 						},

// []*ast.Field{
// 									&ast.Field{
// 										Type: &ast.InterfaceType{
// 											Methods: &ast.FieldList{
// 												List: nil,
// 											},
// 										},
// 									},
// 								}

// []*ast.Field{
// 									&ast.Field{
// 										Type: &ast.Ident{Name: "error"},
// 									},
// 								}
