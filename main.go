package main

import (
	"os"

	"github.com/cialldoin/genDBModels/cli"
)

func main() {
	// config := viper.New()
	// config.SetConfigType("toml")
	// configPath := "test.config.toml"
	// configFile, err := os.Open(configPath)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// err = config.ReadConfig(configFile)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// // fmt.Println(config.AllKeys())
	// fmt.Println(config.GetInt("level.build.time"))
	os.Exit(cli.Run())
}
